package StepsDefinition;

import Pages.HomePage;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.Então;
import io.cucumber.java.pt.Quando;

public class HomeSteps {
    HomePage homePage = new HomePage();

    @Dado("que eu esteja na pagina inicial")
    public void queEuEstejaNaPaginaInicial() throws Exception {
        homePage.validarPaginaInicial();
    }

    @Então("eu mudo a versão para {string}")
    public void euMudoAVersãoPara(String text) throws Exception {
        homePage.mudarVersao(text);
    }

    @Quando("clico no botão Add Customer")
    public void clicoNoBotãoAddCustomer() throws Exception {
        homePage.clicarBtnAddCustomer();
    }

}
