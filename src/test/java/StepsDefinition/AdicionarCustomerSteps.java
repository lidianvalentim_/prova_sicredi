package StepsDefinition;

import Pages.AddCustomerPage;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Então;

public class AdicionarCustomerSteps {
    AddCustomerPage adicionarCustomerPage = new AddCustomerPage();

    @E("preencho com as informações do Customer:")
    public void preenchoComAsInformações(DataTable table) throws Exception {
        adicionarCustomerPage.preencherCampos(table);
        adicionarCustomerPage.clicarBtnSave();

    }

    @Então("devo visualizar uma mensagem de sucesso")
    public void devoVisualizarUmaMensagemDeSucesso() throws Exception {
        adicionarCustomerPage.validarMensagemSucesso();
    }

}
