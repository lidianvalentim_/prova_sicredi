#language: pt
Funcionalidade: Adicionar Customer com informações atráves de um formulário

  Como usuário eu gostaria de adicionar um Customer ao sistema.

  Contexto:
    Dado que eu esteja na pagina inicial
    Então eu mudo a versão para "bootstrap_theme_v4"

  @CT01 @AdcionarCustomer @FirstTest @SMOKE
  Cenário: Adicionar Customer
    Quando clico no botão Add Customer
    Então preencho com as informações do Customer:
      | campo            | valor                 |
      | Name             | Teste Sicredi         |
      | LastName         | Teste                 |
      | ContactFirstName | Lucas Neves           |
      | Phone            | 519999-9999           |
      | Address1         | Av Assis Brasil, 3970 |
      | Address2         | Torre D               |
      | City             | Porto Alegre          |
      | State            | RS                    |
      | PostalCode       | 91000-00              |
      | Country          | Brasil                |
      | fromEmployeer    | Fixter                |
      | creditLimit      | 200                   |
    Então devo visualizar uma mensagem de sucesso
